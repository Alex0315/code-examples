#ifndef TABLE_H_INCLUDED
#define TABLE_H_INCLUDED

#include <QTableWidget>


/** Struct holding one waypoint data set
 */
typedef struct {
	double lat;
	double lon;
	double alt;
	QString rem;
} wayPoint_t;

class Table
      : public QTableWidget
{
	Q_OBJECT

private:
	// use enumerations for a clear identification of the table columns
	enum{col_lat, col_lon, col_alt, col_rem, col_cnt};

	// cell prototypes - one for each type
	QTableWidgetItem *tItemProtoDeg;
	QTableWidgetItem *tItemProtoAlt;
	QTableWidgetItem *tItemProtoRem;

public:
	const int tableWidth = 500;

public:
	explicit Table(QWidget *parent = nullptr);

	wayPoint_t getRowData(int row, bool *ok=Q_NULLPTR) const;

signals:

public slots:
	/**
	 * @brief add a new row to the table
	 */
	void tableAddNewRow();
private slots:
	/**
	 * @brief Check data input and format cell entries
	 * @param row
	 * @param col
	 */
	void h_dataChanged(int row, int col);
};

#endif // TABLE_H_INCLUDED
