#include "mainwindow.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent)
   : QMainWindow(parent)
{
	auto cwidget = new QWidget();
	setCentralWidget(cwidget);
	auto mlayout = new QVBoxLayout(cwidget);

	table = new Table(this);
	mlayout->addWidget(table);

	auto btn_addRow = new QPushButton("Add Row");
	auto btn_readTable = new QPushButton("Read Table");
	auto btn_layout = new QHBoxLayout();
	btn_layout->addWidget(btn_addRow);
	btn_layout->addWidget(btn_readTable);
	mlayout->addLayout(btn_layout);

	auto recLabel = new QLabel("Received data:");
	tableDataOutput = new QTextEdit();
	mlayout->addWidget(recLabel);
	mlayout->addWidget(tableDataOutput);

	/* connect buttons */
	connect( btn_addRow,
	         &QPushButton::clicked,
	         table,
	         &Table::tableAddNewRow );

	connect( btn_readTable,
	         &QPushButton::clicked,
	         this,
	         &MainWindow::handle_btn_readTable_clicked );

	resize(table->tableWidth+35,500);
}

MainWindow::~MainWindow()
{

}

void
MainWindow::handle_btn_readTable_clicked()
{
	tableDataOutput->clear();

	wayPoint_t data;
	QString text;

	for(int row=0; row<table->rowCount();row++){
		data = table->getRowData(row);
		text = "Lat: " + QString::number(data.lat,'f',6)
		       + " | Lon: " + QString::number(data.lon,'f',6)
		       + " | Alt: " + QString::number(data.alt,'f',0)
		       + " | Remarks: " + data.rem;
		tableDataOutput->append(text);
	}
}
