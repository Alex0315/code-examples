#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED

#include <QMainWindow>
#include <QTextEdit>

#include "table.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

private:
	Table *table;
	QTextEdit *tableDataOutput;

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:

public slots:
	void handle_btn_readTable_clicked();
};

#endif // MAINWINDOW_H_INCLUDED
