QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 011_Table
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
    main.cpp \
    mainwindow.cpp \
    table.cpp

HEADERS += \
    mainwindow.h \
    table.h
