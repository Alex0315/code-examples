#include "table.h"

#include <QTableWidgetItem>

Table::Table(QWidget *parent)
   : QTableWidget(parent)
{
	// set up columns
	setColumnCount(col_cnt);

	setColumnWidth(col_lat,2*tableWidth/10);
	setColumnWidth(col_lon,2*tableWidth/10);
	setColumnWidth(col_alt,2*tableWidth/10);
	setColumnWidth(col_rem,4*tableWidth/10);

	// set up table header
	QStringList m_TableHeader;
	m_TableHeader << "lat" << "lon" << "alt" << "remarks";
	setHorizontalHeaderLabels(m_TableHeader);

	// set up cell prototypes
	tItemProtoDeg = new QTableWidgetItem("0");
	tItemProtoDeg->setTextAlignment(Qt::AlignVCenter|Qt::AlignRight);
	tItemProtoAlt = new QTableWidgetItem("0");
	tItemProtoAlt->setTextAlignment(Qt::AlignVCenter|Qt::AlignRight);
	tItemProtoRem = new QTableWidgetItem();
	tItemProtoRem->setTextAlignment(Qt::AlignVCenter|Qt::AlignLeft);

	// add first row
	tableAddNewRow();

	// connect handler to handle data inputs
	connect( this,
	         &QTableWidget::cellChanged,
	         this,
	         &Table::h_dataChanged );
}

void
Table::tableAddNewRow()
{
	auto _rowCount = rowCount();
	setRowCount(_rowCount+1);

	auto newItemLat = tItemProtoDeg->clone();
	setItem(_rowCount, col_lat, newItemLat);
	auto newItemLon = tItemProtoDeg->clone();
	setItem(_rowCount, col_lon, newItemLon);
	auto newItemAlt = tItemProtoAlt->clone();
	setItem(_rowCount, col_alt, newItemAlt);
	auto newItemRem = tItemProtoRem->clone();
	setItem(_rowCount, col_rem, newItemRem);
}

void
Table::h_dataChanged(int row, int col)
{
	auto it = item(row,col);
	bool ok;

	auto num = it->text().toDouble(&ok);

	switch(col){
	case col_lat:
	case col_lon:
		if(ok){
			it->setText(QString::number(num,'f',6));
		} else {
			it->setText("NaN");
		}
		return;
	case col_alt:
		if(ok){
			it->setText(QString::number(num,'f',0));
		} else {
			it->setText("NaN");
		}
		return;
	}
}

wayPoint_t
Table::getRowData(int row, bool *ok) const
{
	wayPoint_t wp;
	bool ret;
	if(ok==Q_NULLPTR){
		ok = new bool;
	}
	*ok = true;

	if(row < rowCount()){
		wp.lat = item(row,col_lat)->text().toDouble(&ret);
		if(!ret){
			*ok &= false;
		}
		wp.lon = item(row,col_lon)->text().toDouble(&ret);
		if(!ret){
			*ok &= false;
		}
		wp.alt = item(row,col_alt)->text().toDouble(&ret);
		if(!ret){
			*ok &= false;
		}
		wp.rem = item(row,col_rem)->text();
	} else {
		*ok = false;
	}

	return wp;
}
