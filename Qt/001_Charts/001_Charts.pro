greaterThan(QT_MAJOR_VERSION, 4):

QT += core gui
QT += widgets
QT += charts

TARGET = 001_Charts
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
   main.cpp \
   mainwindow.cpp

HEADERS += \
   mainwindow.h
