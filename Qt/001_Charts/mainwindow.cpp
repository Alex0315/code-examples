#include "mainwindow.h"

#include <QGridLayout>
#include <QPushButton>
#include <QtCharts>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>

MainWindow::MainWindow(QWidget *parent)
   : QMainWindow(parent)
{
	auto btnClose = new QPushButton("Quit");
	/* provide a signal to make the clicked event accessible outside of this
	 * class */
	connect( btnClose,
	         &QPushButton::clicked,
	         this,
	         &MainWindow::btnCloseClicked );

	/* create a dataset */
	auto series = new QtCharts::QLineSeries();
	series->append(0, 6);
	series->append(2, 4);
	series->append(3, 8);
	series->append(7, 4);
	series->append(10, 5);
    /* or using the stream operator */
	*series << QPointF(11, 1)
	        << QPointF(13, 3)
	        << QPointF(17, 6)
	        << QPointF(18, 3)
	        << QPointF(20, 2);

	/* create a draw frame */
	auto chart = new QtCharts::QChart();
	chart->resize(600,300);
	chart->legend()->hide();
	chart->addSeries(series);
	chart->createDefaultAxes();
	chart->setTitle("Simple line chart example");

	/* show frame with lines */
	auto chartView = new QtCharts::QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	/* needed to carry the grid layout */
	auto cwidged = new QWidget(this);
	this->setCentralWidget(cwidged);

	/* arrange object from above */
	auto grid = new QGridLayout(cwidged);
	grid->addWidget( chartView, 1,1);
	grid->addWidget( btnClose, 2,1);
}

MainWindow::~MainWindow()
{

}
