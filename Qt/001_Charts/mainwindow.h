#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MainWindow : public QMainWindow
{
	 Q_OBJECT

public:
	 MainWindow(QWidget *parent = 0);
	 ~MainWindow();

signals:
	 void btnCloseClicked();

};

#endif // MAINWINDOW_H
