#ifndef STOPBITSELECTOR_H_INCLUDED
#define STOPBITSELECTOR_H_INCLUDED

#include <QComboBox>
#include <QtSerialPort/QSerialPort>

class StopBitSelector
      : public QComboBox
{
public:
	StopBitSelector();

	QSerialPort::StopBits
	currentData()
	{
		return (QSerialPort::StopBits)itemData(currentIndex()).toUInt();
	}
};

#endif // STOPBITSELECTOR_H_INCLUDED
