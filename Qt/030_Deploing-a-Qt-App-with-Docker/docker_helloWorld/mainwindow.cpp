#include "mainwindow.h"

#include <QGridLayout>
#include <QPushButton>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle("Hello World");
    auto cwidget = new QWidget();
    this->setCentralWidget(cwidget);
    auto mLayout = new QGridLayout(cwidget);

    auto label = new QLabel("Hello World!");
    mLayout->addWidget(label);

    auto button = new QPushButton("Click");
    mLayout->addWidget(button);


    label_cnt = new QLabel(QString::number(0));
    mLayout->addWidget(label_cnt);


    connect( button,
             &QPushButton::clicked,
             this,
             &MainWindow::handle_button_clicked);

}

MainWindow::~MainWindow()
{
}

void
MainWindow::handle_button_clicked()
{
    auto cnt = label_cnt->text().toLong();
    label_cnt->setText(QString::number(cnt+3));
}
