#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED

#include <QMainWindow>
#include <QLabel>

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QLabel *label_cnt;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void handle_button_clicked();
};
#endif // MAINWINDOW_H_INCLUDED
