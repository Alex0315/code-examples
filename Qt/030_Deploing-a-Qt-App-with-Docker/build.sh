#/bin/bash

QTVERSION=1.0
TAG="qtondocker"

PROJECTDIR="docker_helloWorld"
PROJECTNAME="docker_helloWorld"

docker build \
	--rm \
	-t $TAG \
	--build-arg PROJECTDIR=$PROJECTDIR \
	--build-arg PROJECTNAME=$PROJECTNAME \
	-f Dockerfile \
	.
