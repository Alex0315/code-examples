# Building and Deploying a Qt-Project using Docker

This example demonstrates how a Qt project can be developed for example with Qt Creator and then deployed in a Docker container.


## Start with a project the Qt way

First a Qt project is created in the usual way. In the directory *docker_helloWorld* contains a small Hello-World program, which has a button to increment a count value.

## Building and Deploying the Qt-Project in Docker

To build a Docker image, a Docker configuration file (*Dockerfile*) is required. A multi-stage build will be used which allows to selectively copy artifacts from one stage to another, leaving behind everything is not want in the final image. In addition, an in-container build respectively the multi-level build ensures that the program built in the container is compatible with the libraries in the container and that all necessary ones are provided.

The base stage (`qt-base`) comprises all libraries that are also required at run-time.

The stage for building the application (`qt-builder`) is derived from the base stage (`qt-base`), adds build dependencies and does the translation. The source files are copied from the host directory (`PROJECTDIR`) to the container. The built program is then located in `/opt/${PROJECTNAME}/bin`.

Finally, a stage is created to which only the finished binaries are copied from `qt-builder`. In addition, settings are made to run the program when the image is launched.

With *build.sh* there is a script to call the building process. Note that `docker build` only rebuilds layers if changes have been made in *Dockerfile*. There is no automatism based on changes to the source code. You may have to work with the `--force` option here or remove the old images..

## Running the Docker container

With *run.sh* a run script is provided. This binds the X-Server of the host to the container in order to be able to display the application.
