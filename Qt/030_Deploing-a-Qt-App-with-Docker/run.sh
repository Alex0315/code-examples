#/bin/bash

xhost +

CURTAG="qtondocker"

docker run \
	-it \
	--rm \
	--env=LOCAL_USER_ID="$(id -u)" \
	-e DISPLAY=unix$DISPLAY \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	${CURTAG}
