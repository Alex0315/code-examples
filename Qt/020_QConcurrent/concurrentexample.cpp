#include "concurrentexample.h"

#include <QtConcurrent/QtConcurrent>


ConcurrentExample::ConcurrentExample(QObject *parent)
   : QObject(parent)
{
	for(int i=0; i<1e6; i++){
		this->dataList.append(i);
		this->dataVector.append(i);
	}
}


void
ConcurrentExample::run()
{

	// the map function has the following signature
	//     void function(T &t)
	// Note:
	//   - the results will written to t - so t may not be const
	//   - there is no return value (void)
	//   - T needs to be the same as of the list/vector content
	auto mapFun = [](int& t){
		t *= 2;
	};


	// the mapped function has the following signature
	//     U function(const T &t)
	// Note:
	//   - the results will returned (t stays untuched)
	//   - T needs to be the same as of the list/vector content
	std::function<int(const int&)> mappedFun = [](const int& d){
		return d*2;
	};


	// the reduce function has the following signature
	//     V function(T &result, const U &intermediate)
	std::function<void(double&, const int&)> reduceFun =
	      [](double &res, const int& intermed){
		res = (res+static_cast<double>(intermed))/2.0;
	};



	// apply mapFun to each element of dataList and store the output
	// to r01
	auto time_start = QDateTime::currentDateTime();
	auto r01 = QtConcurrent::blockingMapped(dataList, mappedFun);
	auto time_end = QDateTime::currentDateTime();
	auto time_delta = time_end.toMSecsSinceEpoch()
	                  - time_start.toMSecsSinceEpoch();
	qInfo("QtConcurrent::blockingMapped(dataList, ...) takes %lld ms", time_delta);



	// Make a copy of dataList in r02 to avaoid that dataList will
	// overwritten. Than apply mappedFun to each element of r02 where
	// the results overwrite the content of r02.
	auto r02 = QList<int>();
	r02.append(dataList);
	time_start = QDateTime::currentDateTime();
	QtConcurrent::blockingMap(r02, mapFun);
	time_end = QDateTime::currentDateTime();
	time_delta = time_end.toMSecsSinceEpoch()
	           - time_start.toMSecsSinceEpoch();
	qInfo("QtConcurrent::blockingMap(dataList, ...) takes %lld ms", time_delta);



	// Same as for r01 with applying reduceFun to all resulting
	// elements of mappedFun in order to reduce all elements
	// to one.
	time_start = QDateTime::currentDateTime();
	auto r03 = QtConcurrent::blockingMappedReduced<double>(
	                dataList, mappedFun, reduceFun,
	                QtConcurrent::OrderedReduce);
	time_end = QDateTime::currentDateTime();
	time_delta = time_end.toMSecsSinceEpoch()
	           - time_start.toMSecsSinceEpoch();
	qInfo("%f", r03);
	qInfo("QtConcurrent::blockingMappedReduced(dataList, ...) takes %lld ms", time_delta);




	// apply mapFun to each element of dataVector and store the output
	// to r11
	time_start = QDateTime::currentDateTime();
	auto r11 = QtConcurrent::blockingMapped(dataVector, mappedFun);
	time_end = QDateTime::currentDateTime();
	time_delta = time_end.toMSecsSinceEpoch()
	           - time_start.toMSecsSinceEpoch();
	qInfo("QtConcurrent::blockingMapped(dataVector, ...) takes %lld ms", time_delta);



	// Make a copy of dataVector in r12 to avaoid that dataVector will
	// overwritten. Than apply mappedFun to each element of r12 where
	// the results overwrite the content of r12.
	auto r12 = QVector<int>();
	r12.append(dataVector);
	time_start = QDateTime::currentDateTime();
	QtConcurrent::blockingMap(r12, mapFun);
	time_end = QDateTime::currentDateTime();
	time_delta = time_end.toMSecsSinceEpoch()
	           - time_start.toMSecsSinceEpoch();
	qInfo("QtConcurrent::blockingMap(dataVector, ...) takes %lld ms", time_delta);



	// Same as for r11 with applying reduceFun to all resulting
	// elements of mappedFun in order to reduce all elements
	// to one.
	time_start = QDateTime::currentDateTime();
	auto r13 = QtConcurrent::blockingMappedReduced<double>(
	                dataVector, mappedFun, reduceFun,
	                QtConcurrent::OrderedReduce);
	time_end = QDateTime::currentDateTime();
	time_delta = time_end.toMSecsSinceEpoch()
	           - time_start.toMSecsSinceEpoch();
	qInfo("%f", r13);
	qInfo("QtConcurrent::blockingMappedReduced(dataVector, ...) takes %lld ms", time_delta);



	emit terminate();
}
