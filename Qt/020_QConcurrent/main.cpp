#include <QCoreApplication>
#include <QTimer>
#include "concurrentexample.h"


int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    auto concurrentExample = new ConcurrentExample(&app);

    QObject::connect(
                concurrentExample, SIGNAL(terminate()),
                &app,SLOT(quit()));

    // This will run the task from the application event loop.
    QTimer::singleShot(0, concurrentExample, SLOT(run()));

    return app.exec();
}
