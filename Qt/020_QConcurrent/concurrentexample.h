#ifndef CONCURRENTEXAMPLE_H_INCLUDED
#define CONCURRENTEXAMPLE_H_INCLUDED

#include <QObject>
#include <QVector>


class ConcurrentExample
        : public QObject
{
    Q_OBJECT

    QList<int> dataList;
    QVector<int> dataVector;

public:
    explicit ConcurrentExample(QObject *parent = nullptr);

public slots:
    void run();

signals:
    void terminate();
};

#endif // CONCURRENTEXAMPLE_H_INCLUDED
