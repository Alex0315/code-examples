#include "mainwindow.h"

#include <QWidget>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent)
   : QMainWindow(parent)
{

	auto cwidget = new QWidget();
	setCentralWidget(cwidget);
	auto mainLayout = new QVBoxLayout(cwidget);

	outTextRaw = new QTextEdit();
	mainLayout->addWidget(outTextRaw);

	joystick = new Joystick(this);

	connect( joystick,
	         &Joystick::axisMoved,
	         this,
	         &MainWindow::handle_axisMoved );

	connect( joystick,
	         &Joystick::buttonPressed,
	         this,
	         &MainWindow::handle_buttonPressed );
}

MainWindow::~MainWindow()
{
	disconnect( joystick,
	            &Joystick::axisMoved,
	            this,
	            &MainWindow::handle_axisMoved );

	disconnect( joystick,
	            &Joystick::buttonPressed,
	            this,
	            &MainWindow::handle_buttonPressed );
}

void
MainWindow::handle_axisMoved(quint8 number, qint16 value)
{
	QString text = "axis "
	               + QString::number(number)
	               + " value: "
	               + QString::number(value);
	outTextRaw->append(text);
}

void
MainWindow::handle_buttonPressed(quint8 number, qint16 value)
{
	QString text = "button "
	               + QString::number(number)
	               + " value: "
	               + QString::number(value);
	outTextRaw->append(text);
}
