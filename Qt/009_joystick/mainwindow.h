#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED

#include <QMainWindow>
#include <QTextEdit>

#include "joystick.h"

class MainWindow
      : public QMainWindow
{
	Q_OBJECT

	Joystick *joystick;
	QTextEdit *outTextRaw;

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void handle_buttonPressed(quint8 number, qint16 value);
	void handle_axisMoved(quint8 number, qint16 value);

};

#endif // MAINWINDOW_H_INCLUDED
