greaterThan(QT_MAJOR_VERSION, 4):

QT += core gui
QT += widgets

TARGET = 009_joystick
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    joystick.cpp

HEADERS += \
    mainwindow.h \
    joystick.h
