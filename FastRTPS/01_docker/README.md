# Running FastRTPS *HelloWorld* in Docker containers

This example demonstrates how to set up [eProsima FastRTPS](https://www.eprosima.com/index.php/products-all/eprosima-fast-rtps) within a [Docker](https://www.docker.com) image and how to build and run the the *HelloWorld* example provided within the sources.

## Set up a FastRTPS core image

First, we set up a FastRTPS core image, which concludes all dependencies of FastRTPS and complete installation of FastRTPS from the source. The core image enables that we can reuse the core image to build up different containers based on it (We will do so in the next step.). With this image, it is possible to build FastRTPS applications, generate code from IDL definitions and execute FastRTPS applications.

The Dockerfile *Dockerfiles/fastrtps-core* comprises the necessary steps following [eProsimas installation manual](https://eprosima-fast-rtps.readthedocs.io/en/latest/sources.html).

To build the image run :
```
docker build \
    --rm \
    -t fastrtps-core \
    -f Dockerfiles/fastrtps-core \
    .
```
__The image comprises:__
  - [eProsima FastRTPS](https://www.eprosima.com/index.php/products-all/eprosima-fast-rtps)
  - [eProsima FastCDR](https://github.com/eProsima/Fast-CDR)
  - [eProsima FastRTPSGen](https://eprosima-fast-rtps.readthedocs.io/en/latest/geninfo.html)
  - [Foonathan memory](https://foonathan.net/memory/)
  - [GIT](https://git-scm.com)
  - [GCC](https://gcc.gnu.org),
    [G++](https://www.cprogramming.com/g++.html)
  - [CMake](https://cmake.org)
  - [Colcon](https://colcon.readthedocs.io/en/released/)
  - [Gradle](https://gradle.org)
  - [openJDK, openJRE](https://openjdk.java.net)


## Set up an Image with eProsima FastRTPS *HelloWorld* examples

Now we are going to set up an example image based on the core image described above.

eProsima provides a *HelloWorld* example which can be in the role of a publisher or a subscriber. The publisher sends a counter value, and the subscriber receives them. We want them to run on different containers to demonstrate how it would be if we set up a network of different computers (nodes) which shall communicate with each other using FastRTPS.

The Dockerfile *Dockerfiles/fastrtps-examples* uses `fastrtps-core` as base and clones the sources from eProsimas git repository to */opt/Fast-RTPS/*. This directory comprises the folder *examples/*. The content of *examples/* will exclusively build into */opt/build/* and installed. Afterwards, the folders added to */opt/* is removed to keep the image small. All build examples are then installed to */usr/local/examples/C++/*.

To build the image run :
```
docker build \
    --rm \
    -t fastrtps-examples \
    -f Dockerfiles/fastrtps-examples \
    .
```

The example image *fastrtps-examples* is now ready to use.


## Running the examples

As the image *fastrtps-examples* now comprises all provided examples ready to run, we can now run them.

First, we run a publisher container on a terminal:
```
docker run -it --rm fastrtps-examples /usr/local/examples/C++/HelloWorldExample/HelloWorldExample publisher
```
And then we run a subscriber container on another terminal:
```
docker run -it --rm fastrtps-examples /usr/local/examples/C++/HelloWorldExample/HelloWorldExample subscriber
```
and a second subscriber container on another terminal:
```
docker run -it --rm fastrtps-examples /usr/local/examples/C++/HelloWorldExample/HelloWorldExample subscriber
```
If all goes right a permanent printout with up counting numbers shall be visible on every terminal. To stop the programmes hit enter in every terminal.


## For convenience

The bash script *build.sh* comprises the build calls for *fastrtps-core* and *fastrtps-examples*. The bash script *run.sh* runs the example described above.

To use them:
```
bash build.sh
bash run.sh
```

## Conclusion and why this is cool

1. This demonstration shows how easy to use FastRTPS with Docker.

2. The Docker images keep the host computer clean. All the needed installations happen within the container, and they can rebuild and deleted as wanted.

3. As Docker provides a separate network (`docker`), the traffic between the nodes can quickly be investigated, i.e. with [Wireshark](https://www.wireshark.org), without disturbing the real (host) network. But it can expand to the host or directly to the physical network as well.

4. Using [Docker-compose](https://docs.docker.com/compose/) or [Docker-swarm](https://docs.docker.com/engine/swarm/), it is easy to deploy tiny to huge, uniform, reproducible network constellations. They can also be deployed across multiple computers.

5. It is easy to share the set up with other peoples. Share the scripts or the images - that's all. There is no need for any installation instead of Docker itself. That is good for development, production and runtime.
