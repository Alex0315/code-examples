#/bin/bash

# set here the call for a terminal (gnome-termial, xterm, terminal, ...)
TERMINALCALL='gnome-terminal -e'

# run the HelloWorld publisher
${TERMINALCALL} 'docker run -it --rm fastrtps-examples /usr/local/examples/C++/HelloWorldExample/HelloWorldExample publisher ; echo exit'

# run the HelloWorld subscriber
${TERMINALCALL} 'docker run -it --rm fastrtps-examples /usr/local/examples/C++/HelloWorldExample/HelloWorldExample subscriber'

${TERMINALCALL} 'docker run -it --rm fastrtps-examples /usr/local/examples/C++/HelloWorldExample/HelloWorldExample subscriber'
