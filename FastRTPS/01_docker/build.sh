#/bin/bash

docker build \
	--rm \
	-t fastrtps-core \
	-f Dockerfiles/fastrtps-core \
	.

docker build \
	--rm \
	-t fastrtps-examples \
	-f Dockerfiles/fastrtps-examples \
	.
